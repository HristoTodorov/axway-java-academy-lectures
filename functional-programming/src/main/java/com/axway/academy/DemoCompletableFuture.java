package com.axway.academy;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DemoCompletableFuture {
        public static void main(String[] args) throws Exception {
        CSVParser parser = CSVParser.parse(Paths.get("/Users/hgtodorov/Downloads/Data7602.csv").toFile(),
                Charset.defaultCharset(), CSVFormat.DEFAULT.withHeader());
        ExecutorService executorService = Executors.newCachedThreadPool();
        CompletableFuture<List<CSVRecord>> records = CompletableFuture.supplyAsync(() -> {
            try {
                return parser.getRecords();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            return null;
        }, executorService);
        CompletableFuture<IntSummaryStatistics> geo_count = records.thenComposeAsync(csvRecords ->
                CompletableFuture.supplyAsync(() -> csvRecords.stream().mapToInt(
                        value -> Integer.parseInt(value.get("geo_count"))).summaryStatistics()), executorService);
        CompletableFuture<IntSummaryStatistics> ec_count = records.thenComposeAsync(csvRecords ->
                CompletableFuture.supplyAsync(() -> csvRecords.stream().mapToInt(
                        value -> Integer.parseInt(value.get("ec_count"))).summaryStatistics()), executorService);
        ec_count.thenCombine(geo_count, (first, second) -> {
            IntSummaryStatistics result = new IntSummaryStatistics(
                    first.getCount() + second.getCount(),
                    Math.min(first.getMin(), second.getMin()),
                    Math.max(first.getMax(), second.getMax()),
                    first.getSum() + second.getSum());
            System.out.println(result);
            return result;
        });
        System.out.println("End");
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }
}
