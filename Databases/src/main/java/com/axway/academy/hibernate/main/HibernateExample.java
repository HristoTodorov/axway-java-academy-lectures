package com.axway.academy.hibernate.main;

import java.util.List;
import java.util.Scanner;

import com.axway.academy.hibernate.api.UserManager;
import com.axway.academy.hibernate.beans.Item;
import com.axway.academy.hibernate.beans.User;

/**
 * Hibernate example.
 * The example below does the following:
 * 1. Creates a user and persists it into the database.
 * 2. Lists all users currently added in the database.
 * 3. Gets a user by ID from the database.
 * 4. Creates an item and persists it into the database.
 * 
 * @author aandreev
 *
 */
public class HibernateExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Lets create a new user.");

        System.out.println("Enter name");
        String name = scan.nextLine();
        System.out.println("Enter age");
        int age = Integer.parseInt(scan.nextLine());
        System.out.println("Enter vegan[1/0]");
        boolean vegan = scan.nextLine().equals("1") ? true : false;

        User user = new User(name, age, vegan);
        
        // create an instance of the manager
        // it creates a session factory
        UserManager manager = new UserManager();
        
        // persist the user to the database
        manager.addUser(user);
        
        System.out.println("===============");
        System.out.println("List all users");
        List<User> users = manager.listUsers();
        for (User u : users) {
            System.out.println(u.getId() + " " + user.getName());
        }

        System.out.println("====================");
        System.out.println("Lets create an item");
        System.out.println("Enter ID of the user to which the item belongs");
        Long userId = Long.parseLong(scan.nextLine());
        
        // get a specific user from database by ID
        User userFromList = manager.getUser(userId);
        
        System.out.println("Enter name of the item");
        String itemName = scan.nextLine();
        System.out.println("Enter price of the item");
        int price = Integer.parseInt(scan.nextLine());

        // create an item
        Item item = new Item(itemName, price);
        item.setUser(user);
        
        // persist the item in the database
        manager.addItem(item);
    }

}
