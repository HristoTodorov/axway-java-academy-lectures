package com.axway.academy.hibernate.api;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.axway.academy.hibernate.beans.Item;
import com.axway.academy.hibernate.beans.User;

/**
 * Executes various CRUD operations using Hibernate.
 * 
 * @author aandreev
 *
 */
public class UserManager {

    /**
     * Create the session factory. This is where Hibernate configuration is read.
     */
    private static SessionFactory factory = new Configuration().configure().addAnnotatedClass(User.class)
            .addAnnotatedClass(Item.class).buildSessionFactory();

    /**
     * Default constructor.
     */
    public UserManager() {

    }

    /**
     * Persists a user to the database.
     * 
     * @param user - user to be persisted.
     */
    public void addUser(User user) {
        Session session = null;
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            Long id = (Long) session.save(user);
            tx.commit();
            System.out.println("A user with ID " + id + " was created.");
        } catch (HibernateException e) {
            e.printStackTrace();
            tx.rollback();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Persists an item in the database.
     * 
     * @param item - item to be persisted
     */
    public void addItem(Item item) {
        Session session = null;
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            Long id = (Long) session.save(item);
            tx.commit();
            System.out.println("An item with ID " + id + " was created.");
        } catch (HibernateException e) {
            e.printStackTrace();
            tx.rollback();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Lists all users from the database.
     * 
     * @return a list with users
     */
    public List<User> listUsers() {
        Session session = null;
        Transaction tx = null;
        List<User> users = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            users = (List<User>) session.createQuery("from User").list();
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            tx.rollback();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return users;
    }

    /**
     * Updates an already existing user. The code first gets the user by ID and then updates its age.
     * 
     * @param id - ID of the user
     * @param newAge - new age to be updated
     */
    public void updateUser(long id, int newAge) {
        Session session = null;
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            User user = session.get(User.class, id);
            user.setAge(newAge);
            session.update(user);
            tx.commit();
            System.out.println("A user with ID " + id + " was created.");
        } catch (HibernateException e) {
            e.printStackTrace();
            tx.rollback();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Gets a user from the database by ID.
     * 
     * @param id - ID of the user
     * @return the user
     */
    public User getUser(long id) {
        Session session = null;
        Transaction tx = null;
        User user = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            user = session.get(User.class, id);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            tx.rollback();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return user;
    }

}
