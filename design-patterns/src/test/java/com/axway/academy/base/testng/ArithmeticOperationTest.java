package com.axway.academy.base.testng;

import com.axway.academy.base.ArithmeticOperation;
import com.axway.academy.base.ArithmeticOperationFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class ArithmeticOperationTest {

    private static ArithmeticOperationFactory arithmeticOperationFactory;

    @BeforeClass
    public static void before() {
        System.out.println("Before class.");
        arithmeticOperationFactory = ArithmeticOperationFactory.getInstance();
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("Before the test.");
    }

    @BeforeMethod
    public void beforeMethod(Method method, Object[] testData) {
        System.out.println(String.format("Before method: %s.", method));
        for (Object data : testData) {
            System.out.println(
                    String.format("Test data for the method: %f.", data));
        }
    }

    @DataProvider(name = "addOperation")
    public Object[][] provideAddOperation() {
        return new Object[][] {
                {2.0, 3.0, 5.0},
                {0.0, 0.0, 0.0},
                {2.0, -3.0, -1.0}
        };
    }

    @Test(dataProvider = "addOperation")
    public void addOperationTest(Double arg1, Double arg2, Double result) {
        ArithmeticOperation sumOperation = arithmeticOperationFactory.sumOperation();
        Assert.assertEquals(result, sumOperation.doOperation(arg1, arg2));
    }


}
