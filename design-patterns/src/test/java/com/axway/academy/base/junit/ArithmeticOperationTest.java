package com.axway.academy.base.junit;

import com.axway.academy.base.ArithmeticOperation;
import com.axway.academy.base.ArithmeticOperationFactory;
import org.junit.jupiter.api.*;

class ArithmeticOperationTest {

    private static ArithmeticOperationFactory arithmeticOperationFactory;

    @BeforeAll
    static void beforeAll() {
        System.out.println("Test start.");
        arithmeticOperationFactory = ArithmeticOperationFactory.getInstance();
    }

    @AfterAll
    static void tearDown() {
        System.out.println("Test ended.");
    }

    @BeforeEach
    void beforeEach(TestInfo info) {
        System.out.println(String.format("Before: %s.", info));
    }

    @AfterEach
    void afterEach(TestInfo info) {
        System.out.println(String.format("After: %s.", info));
    }

    @Test
    void sumOperationTest() {
        ArithmeticOperation sumOperation = arithmeticOperationFactory.sumOperation();
        Assertions.assertEquals(5, sumOperation.doOperation(2, 3));
        Assertions.assertEquals(-1, sumOperation.doOperation(2, -3));
        Assertions.assertEquals(0, sumOperation.doOperation(2, -2));
    }

    @Test
    void powPositiveTest() {
        ArithmeticOperation powOperation = arithmeticOperationFactory.powerOperation();
        Assertions.assertEquals(1, powOperation.doOperation(1, 1));
        Assertions.assertEquals(1, powOperation.doOperation(3, 0));
        Assertions.assertEquals(9, powOperation.doOperation(3, 2));
    }

    @Test
    void powNegativeTest() {
        ArithmeticOperation powOperation = arithmeticOperationFactory.powerOperation();
        Assertions.assertThrows(IllegalArgumentException.class, () -> powOperation.doOperation(0, 0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> powOperation.doOperation(0, 1));
    }
}
