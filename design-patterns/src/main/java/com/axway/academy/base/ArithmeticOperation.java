package com.axway.academy.base;

public interface ArithmeticOperation {
    double doOperation(double arg1, double arg2);
}
