package com.axway.academy.base;

public class ArithmeticOperationFactory {

    private ArithmeticOperationFactory() {}

    private static ArithmeticOperationFactory INSTANCE;

    public synchronized static ArithmeticOperationFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ArithmeticOperationFactory();
        }
        return INSTANCE;
    }

    public ArithmeticOperation sumOperation() {
        return Double::sum;
    }

    public ArithmeticOperation divideOperation() {
        return (arg1, arg2) -> arg1 / arg2;
    }

    public ArithmeticOperation multiplyOperation() {
        return (arg1, arg2) -> arg1 * arg2;
    }

    public ArithmeticOperation subtractionOperation() {
        return (arg1, arg2) -> arg1 - arg2;
    }

    public ArithmeticOperation powerOperation() {
        return (base, exponent) -> {
            if (base == 0) {
                throw new IllegalArgumentException("Base can not be 0!");
            }
            return Math.pow(base, exponent);
        };
    }
}
