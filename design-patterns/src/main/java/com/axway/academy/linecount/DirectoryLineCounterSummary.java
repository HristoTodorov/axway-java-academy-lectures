package com.axway.academy.linecount;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public interface DirectoryLineCounterSummary {
    Set<File> getFilesWithMostLines(String directory) throws IOException;
}
