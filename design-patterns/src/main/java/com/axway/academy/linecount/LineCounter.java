package com.axway.academy.linecount;

import java.io.IOException;

public interface LineCounter {
    int countLine(String file) throws IOException;
}
