package com.axway.academy.linecount;

public interface LineCounterFactory {
    LineCounter getLineCounter();
    DirectoryLineCounterSummary getDirectoryLineCounterSummary();
}
