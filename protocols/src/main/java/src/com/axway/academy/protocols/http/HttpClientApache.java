package com.axway.academy.protocols.http;

import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * An HTTP client that executes a GET request using Apache client.
 * 
 * @author aandreev
 *
 */
public class HttpClientApache {

    public static void main(String[] args) {
        
        // client and request initialization
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://google.com");
        try {
            // executing request
            CloseableHttpResponse response = httpclient.execute(httpGet);
            
            // writing response status line
            System.out.println(response.getStatusLine().toString());
            
            // reading response
            String responseResult = EntityUtils.toString(response.getEntity());
            System.out.println(responseResult);

        } catch (IOException e) {
            System.out.println("Problem executing GET request.");
            e.printStackTrace();        
        } finally {
            try {
                if (httpclient != null) {
                    httpclient.close();
                }
            } catch (IOException e) {
                System.out.println("Cannot clsoe streams.");
                e.printStackTrace();
            }
        }
    }

}
