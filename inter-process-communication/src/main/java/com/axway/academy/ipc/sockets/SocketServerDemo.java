package com.axway.academy.ipc.sockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServerDemo {

    public static final int SERVER_PORT = 12_001;

    public static final String READY_COMMAND = "I'm Ready!";

    public static final String READY_RESPONSE = "Fine, let's the fun begin!";

    public static void main(String[] args) throws Exception {
        try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
            // accepting client request
            while (true) {
                Socket accept = serverSocket.accept();
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(accept.getInputStream()));
                     PrintWriter writer = new PrintWriter(accept.getOutputStream(), true)) {
                    String message = reader.readLine();
                    if (READY_COMMAND.equals(message)) {
                        System.out.println(String.format("Receiving command from client: %s.", READY_COMMAND));
                        System.out.println(String.format("Sending command to client: %s.", READY_RESPONSE));
                        writer.println(READY_RESPONSE);
                    } else {
                        writer.println("ERROR!");
                    }
                }
            }
        }
    }
}
