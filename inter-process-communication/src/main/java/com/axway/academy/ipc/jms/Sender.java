package com.axway.academy.ipc.jms;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class Sender {

    static final String TEST_QUEUE = "__TEST_QUEUE__";

    static final String TEST_TOPIC = "__TEST_TOPIC__";

    public static void main(String[] args) throws Exception {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destinationQueue = session.createQueue(TEST_QUEUE); // session.createTopic(TEST_TOPIC) for topic
        MessageProducer queueProducer = session.createProducer(destinationQueue);
        TextMessage queueMessage = session.createTextMessage("Hello from Queue! This is a first message to ActiveMQ!");
        queueMessage.setIntProperty("age", 10); // set a integer property for this message - age with value 10
        queueProducer.send(queueMessage);

        connection.close();
    }
}
