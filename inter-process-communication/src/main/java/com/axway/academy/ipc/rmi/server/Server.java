package com.axway.academy.ipc.rmi.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public static final int RMI_PORT = 12_000;

    public static final String BIND_NAME = "RemoteAddOperation";

    public static void main(String[] args) throws Exception {
        AddOperationServerImpl instance = new AddOperationServerImpl();
        Registry registry = LocateRegistry.createRegistry(RMI_PORT);
        registry.rebind(BIND_NAME, instance);
    }
}
