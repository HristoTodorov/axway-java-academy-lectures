package com.axway.academy.ipc.jms;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.concurrent.TimeUnit;

public class Receiver {
    public static void main(String[] args) throws Exception {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destination = session.createQueue(Sender.TEST_QUEUE); //session.createTopic(TEST_TOPIC) for topic
        // will accept only messages with age property equals to 10
        MessageConsumer consumer = session.createConsumer(destination, "age = 10");
//        Message message = consumer.receive();
        consumer.setMessageListener(System.out::println); // create a consumer message listener
//        if (message instanceof TextMessage) {
//            TextMessage textMessage = (TextMessage) message;
//            System.out.println("Received message '" + textMessage.getText() + "'");
//        }
        TimeUnit.MINUTES.sleep(1);
        connection.close();
    }
}
